/**
 * Base URL
 * */
export const BASE_BACKEND_URL = 'http://localhost:7070';
export const BASE_IMAGE_URL = 'http://localhost:8086';
export const BASE_LANDING_PAGE = 'http://localhost:4200/home';

export const BASE_ATTENDANCE_URL = BASE_BACKEND_URL + '/attendance';
export const BASE_APPLICATION = BASE_BACKEND_URL + '/application';
export const BASE_CLASSSTATUS = BASE_BACKEND_URL + '/class-status';
export const BASE_ALBUM = BASE_BACKEND_URL + '/albums';
export const BASE_PHOTO = BASE_ALBUM + '/photos';
export const BASE_HYGIENE = BASE_BACKEND_URL + '/child-status/individual';
export const BASE_PRESCRIPTION = BASE_BACKEND_URL + '/prescription';
export const BASE_INDIVI_PRESCRIPTION = BASE_PRESCRIPTION + '/individual';
export const BASE_CHILD_HEALTH = BASE_BACKEND_URL + '/health';
export const BASE_NOTIFICATION = BASE_BACKEND_URL + '/notification';
export const BASE_SYSTEMLOG = BASE_BACKEND_URL + '/log';
export const BASE_NOTI_QUANTITY = BASE_BACKEND_URL + '/count-noti';
export const BASE_CHILDREN_INFO = BASE_BACKEND_URL + '/children/info';

export const PHOTOS_URL = BASE_IMAGE_URL + '/kidcare/';
export const GET_ALBUMS = BASE_ALBUM + '/list';
export const GET_MENU = BASE_CLASSSTATUS + '/classStatus-week';


export const QUERY_PARAM = {
  DATE: 'date'
};
