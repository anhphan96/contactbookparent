"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var environment_1 = require("../../environments/environment");
exports.KEYCLOAK = {
    REALM: environment_1.KEYCLOAK_CONFIG.REALM,
    CLIENT_ID: environment_1.KEYCLOAK_CONFIG.CLIENT_ID,
    AUTH_URL: environment_1.KEYCLOAK_CONFIG.AUTH_URL
};
