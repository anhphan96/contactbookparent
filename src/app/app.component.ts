import {Component, OnInit} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {GlobalConfig, ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {SystemLog} from './_models/system-log';
import {ApplicationService} from './_service/application.service';
import {SystemLogService} from './_service/system-log.service';
import {NotiQuantityService} from './_service/noti-quantity.service';
import {NotiQuantity} from './_models/noti-quantity';
import {PrescriptionService} from './_service/prescription.service';
import {isNullOrUndefined} from 'util';
import {KeycloakService} from "keycloak-angular";
import {GlobalService} from "./_service/global.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'contactbookparent';
  private stompClient;

  constructor(private toast: ToastrService,
              private route: Router,
              private applicaionService: ApplicationService,
              private systemLogService: SystemLogService,
              private prescriptionService: PrescriptionService,
              private notiquantityService: NotiQuantityService,
              private keyCloakService: KeycloakService,
              private globalService: GlobalService) {

  }

  ngOnInit(): void {
    this.toast.toastrConfig.timeOut = 10000;
    // this.keyCloakService.getToken().then((data) => this.initializeAbsentApplicationConnection(data));
  }

  // initializeAbsentApplicationConnection(token: string) {
  //   const ws = new WebSocket('ws://localhost:7070/greet?access_token=bearer%20' + token);
  //   this.stompClient = Stomp.over(ws);
  //   const that = this;
  //   this.stompClient.connect({}, function (frame) {
  //     that.stompClient.subscribe('/application/' + that.globalService.currentChild.id, (message) => {
  //       if (message.body) {
  //         const systemLog = JSON.parse(message.body) as SystemLog;
  //
  //         const applicationId = systemLog.dynamicObject as string;
  //         that.applicaionService.applicationBehaviorSubject.next(applicationId);
  //         that.systemLogService.systemLogSubject.next(systemLog);
  //         that.toast.info(systemLog.content, '').onTap.subscribe(() => {
  //           that.navigate('absent-applications');
  //         });
  //         if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
  //           that.notiquantityService.notiQuanityObject.systemlog++;
  //           that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
  //         }
  //       }
  //     });
  //
  //     that.stompClient.subscribe('/album/' + that.globalService.currentChild.currentClassId, (message) => {
  //       if (message.body) {
  //         const systemLog = JSON.parse(message.body) as SystemLog;
  //         that.systemLogService.systemLogSubject.next(systemLog);
  //         that.toast.info(systemLog.content, '').onTap.subscribe(() => {
  //           that.navigate('albums/list');
  //         });
  //         if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
  //           that.notiquantityService.notiQuanityObject.systemlog++;
  //           that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
  //         }
  //       }
  //     });
  //
  //     that.stompClient.subscribe('/health/' + that.globalService.currentChild.id, (message) => {
  //       if (message.body) {
  //         const systemLog = JSON.parse(message.body) as SystemLog;
  //         that.systemLogService.systemLogSubject.next(systemLog);
  //         that.toast.info(systemLog.content, '').onTap.subscribe(() => {
  //           that.navigate('health');
  //         });
  //         if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
  //           that.notiquantityService.notiQuanityObject.systemlog++;
  //           that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
  //         }
  //       }
  //     });
  //
  //     that.stompClient.subscribe('/prescription/' + that.globalService.currentChild.id, (message) => {
  //       if (message.body) {
  //         const systemLog = JSON.parse(message.body) as SystemLog;
  //         const prescriptionId = systemLog.dynamicObject as string;
  //         that.systemLogService.systemLogSubject.next(systemLog);
  //         that.prescriptionService.prescriptionBehaviorSubject.next(prescriptionId);
  //         that.toast.info(systemLog.content, '').onTap.subscribe(() => {
  //           that.navigate('prescription');
  //         });
  //         if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
  //           that.notiquantityService.notiQuanityObject.systemlog++;
  //           that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
  //         }
  //       }
  //     });
  //   });
  // }

  navigate(url: string) {
    this.route.navigate([url]);
  }
}
