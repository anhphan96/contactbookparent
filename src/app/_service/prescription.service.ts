import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of, Subject} from 'rxjs/index';
import {HttpService} from './http.service';
import {AlertService} from './alert.service';
import {BASE_INDIVI_PRESCRIPTION, BASE_PRESCRIPTION} from '../_constant/path.constant';
import {catchError, map} from 'rxjs/internal/operators';
import {Prescription} from '../_models/prescription';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class PrescriptionService implements Resolve<any> {
  prescriptions: Prescription[];
  prescriptionBehaviorSubject: Subject<Prescription>;

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router,
              private globalService: GlobalService) {
    this.prescriptionBehaviorSubject = new Subject();
  }

  getIndividualPresciptions() {
    return this.httpService.get(BASE_INDIVI_PRESCRIPTION + '/' + this.globalService.currentChild.id);
  }

  createPrescription(prescription: Prescription) {
    return this.httpService.post(BASE_PRESCRIPTION, prescription);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    const date = new Date();
    return this.getIndividualPresciptions().pipe(
      map((data: Prescription[]) => {
        this.alertService.closeAlert();
        this.prescriptions = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/home']);
        return of(err);
      })
    );
  }
}
