import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AlertService} from './alert.service';
import {catchError, map} from 'rxjs/internal/operators';
import {IndividualAttendance} from '../_models/individualAttendance';
import {HttpService} from './http.service';
import {BASE_ATTENDANCE_URL, QUERY_PARAM} from '../_constant/path.constant';
import {HttpParams} from '@angular/common/http';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class AttendanceService implements Resolve<any> {
  public individualAttendances: IndividualAttendance[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private alertService: AlertService,
              private router: Router,
              private httpService: HttpService,
              private globalService: GlobalService) {
  }

  getIndividualAttendances(date: Date) {
    let params = new HttpParams();
    params = params.set(QUERY_PARAM.DATE, (date.getDate()) +
      '-' + (date.getMonth() + 1) +
      '-' + date.getFullYear());
    return this.httpService.getWithParam(BASE_ATTENDANCE_URL + '/'
      + this.globalService.currentChild.currentClassId
      + '/' + this.globalService.currentChild.id, params);
  }

  getData() {
    this.alertService.alertLoading('Loading...');
    return this.getIndividualAttendances(new Date()).pipe(
      map((data: IndividualAttendance[]) => this.individualAttendances = data),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/home']);
        return of(err);
      })
    );
  }
}
