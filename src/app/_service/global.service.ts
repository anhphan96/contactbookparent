import { Injectable } from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {HttpService} from './http.service';
import {Children} from '../_models/children';
import {BASE_CHILDREN_INFO} from '../_constant/path.constant';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public currentChild: Children;
  public listChildren: Children[];
  constructor(private keyCloakService: KeycloakService,
              private httpService: HttpService) {
  }

  public getChildrenInfoByUsername() {
    return this.httpService.get(BASE_CHILDREN_INFO + '/' + this.keyCloakService.getUsername());
  }
}
