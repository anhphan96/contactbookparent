import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs/index';
import {catchError, map} from 'rxjs/internal/operators';
import {AlertService} from './alert.service';
import {HttpService} from './http.service';
import {Health} from '../_models/health';
import {BASE_CHILD_HEALTH} from '../_constant/path.constant';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class HealthService implements Resolve<any> {
  childrenHealthes: Health[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
      return this.getData();
  }

  constructor(private httpService: HttpService,
              private router: Router,
              private alertService: AlertService,
              private globalService: GlobalService) {
  }

  getChildrenHealthes() {
    return this.httpService.get(BASE_CHILD_HEALTH + '/' + this.globalService.currentChild.id);
  }

  saveChildrenHeal(heal: Health) {
    return this.httpService.put(BASE_CHILD_HEALTH + '/' + this.globalService.currentChild.id, heal);
  }

  deleteChildrenHeal(healthId: string) {
    return this.httpService.delete(BASE_CHILD_HEALTH + '/' + healthId);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getChildrenHealthes().pipe(
      map((data: Health[]) => {
        this.childrenHealthes = data;
        this.alertService.closeAlert();
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/teacher']);
        return of(err);
      })
    );
  }
}
