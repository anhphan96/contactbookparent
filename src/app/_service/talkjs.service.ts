import {Injectable} from '@angular/core';
import * as Talk from 'talkjs';
import {BehaviorSubject, Observable} from 'rxjs/index';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AlertService} from './alert.service';
import {isNullOrUndefined} from "util";

@Injectable(
  {providedIn: 'root'}
)
export class TalkJsService implements Resolve<any> {
  private session: Promise<Talk.Session>;
  private currentUsername: string;
  private currentUser: Talk.User;
  public currentUserBehaviourSubject: BehaviorSubject<Talk.User>;

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    if (isNullOrUndefined(this.session)) {
      return this.createSession('Nhat');
    }
    return this.session;
  }

  constructor(private alertService: AlertService) {
    this.currentUserBehaviourSubject = new BehaviorSubject(null);
  }

  createSession(username: string) {
    this.currentUsername = username;
    console.log('Logging in as ' + username + '...');
    this.session = Talk.ready.then(() => {
      this.currentUser = new Talk.User({
        id: username,
        name: username,
        configuration: 'buyer'
      });

      console.log('Logged in.');
      this.currentUserBehaviourSubject.next(this.currentUser);
      return new Talk.Session({
        appId: 'thu0fCOh',
        me: this.currentUser
      });
    }) as Promise<Talk.Session>;
    return this.session;
  }

  getCurrentUsername() {
    return this.currentUsername;
  }

  getCurrentUser() {
    return this.currentUser;
  }

  getSession() {
    return this.session;
  }
}
