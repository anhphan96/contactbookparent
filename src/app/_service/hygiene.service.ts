import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs/index';
import {AlertService} from './alert.service';
import {HttpService} from './http.service';
import {BASE_HYGIENE, QUERY_PARAM} from '../_constant/path.constant';
import {HttpParams} from '@angular/common/http';
import {catchError, map} from 'rxjs/internal/operators';
import {ChildStatus} from '../_models/childStatus';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class HygieneService implements Resolve<any> {
  public childrenStatus: ChildStatus[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router,
              private globalService: GlobalService) {
  }

  getIndividualChildStatus(date: Date) {
    let params = new HttpParams();
    // const currentDay = new Date();
    params = params.set(QUERY_PARAM.DATE, (date.getDate()) +
      '-' + (date.getMonth() + 1) +
      '-' + date.getFullYear());
    return this.httpService.getWithParam(BASE_HYGIENE + '/' + this.globalService.currentChild.id, params);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    const date = new Date();
    return this.getIndividualChildStatus(date).pipe(
      map((data: ChildStatus[]) => {
        this.alertService.closeAlert();
        this.childrenStatus = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/teacher']);
        return of(err);
      })
    );
  }
}
