import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Album} from '../_models/album';
import {GET_ALBUMS} from '../_constant/path.constant';
import {BehaviorSubject, of} from 'rxjs/index';
import {Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {ActivatedRouteSnapshot} from '@angular/router';
import {AlertService} from './alert.service';
import {catchError, map} from 'rxjs/internal/operators';
import {isNullOrUndefined} from "util";
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class AlbumsService implements Resolve<any> {
  selectedIndex = 0;
  albums: BehaviorSubject<Album[]>;
  listAlbums: Album[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (isNullOrUndefined(this.listAlbums)) {
      return this.getData();
    }
    return this.listAlbums;
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router,
              private globalService: GlobalService) {
    this.albums = new BehaviorSubject([]);
  }

  public getAlbums() {
    return this.httpService.get(GET_ALBUMS + '/' + this.globalService.currentChild.currentClassId);
  }

  getData(): any {
    this.alertService.alertLoading('Loading...');
    return this.getAlbums().pipe(
      map((data: Album[]) => {
          this.alertService.closeAlert();
          this.listAlbums = data;
        },
        catchError(err => {
          this.alertService.closeAlert();
          this.router.navigate(['/home']);
          return of(err);
        })
      ));
  }
}
