import {Injectable} from '@angular/core';
import {HttpService} from "./http.service";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {AlertService} from "./alert.service";
import {Observable, of, Subject} from "rxjs/index";
import {BASE_NOTI_QUANTITY} from "../_constant/path.constant";
import {NotiQuantity} from "../_models/noti-quantity";
import {catchError, map} from "rxjs/internal/operators";
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class NotiQuantityService implements Resolve<any> {
  notiQuantity = new Subject();
  notiQuantityForApplication = new Subject();
  notiQuantityForPres = new Subject();
  notiQuanityObject: NotiQuantity;

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private router: Router,
              private globalService: GlobalService) {
  }

  public getNotiQuantity() {
    return this.httpService.get(BASE_NOTI_QUANTITY + '/' +
      this.globalService.currentChild.currentClassId + '/' + this.globalService.currentChild.id);
  }

  public getData() {
    return this.notiQuantity.pipe(
      map((data: NotiQuantity) => {
        this.notiQuanityObject = data;
        console.log('data');
      }),
      catchError(error => {
        this.router.navigate(['/teacher']);
        return of(error);
      })
    );
    // this.httpService.get(BASE_NOTI_QUANTITY + '/' + classId + '/' + childId).subscribe((data: NotiQuantity) => {
    //   this.notiQuantity.next(data);
    // },
    //   error => {
    //     this.router.navigate(['/teacher']);
    //   });
    // this.getNotiQuantity(classId, childId).pipe(
    //   map((data: NotiQuantity) => {
    //     this.notiQuantity.next(data);
    //   }),
    //   catchError(err => {
    //     this.alertService.closeAlert();
    //     this.router.navigate(['/teacher']);
    //     return of(err);
    //   })
    // );
  }
}
