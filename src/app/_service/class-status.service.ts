import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {HttpService} from './http.service';
import {AlertService} from './alert.service';
import {catchError, map} from 'rxjs/operators';
import {BASE_CLASSSTATUS, GET_MENU, QUERY_PARAM} from '../_constant/path.constant';
import {ClassStatus} from '../_models/classStatus';
import {HttpParams} from '@angular/common/http';
import {Menu} from '../_models/menu';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class ClassStatusService implements Resolve<any> {
  classStatus: ClassStatus[];

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router,
              private globalService: GlobalService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  public getClassStatus(classId: string, date: Date): Observable<any> {
    let params = new HttpParams();
    params = params.set(QUERY_PARAM.DATE, (date.getDate() + 1) +
      '-' + (date.getMonth() + 1) +
      '-' + date.getFullYear());
    return this.httpService.getWithParam(GET_MENU + '/' + classId, params);
  }

  public getClassStatusInMonth(date: Date): Observable<any> {
    let params = new HttpParams();
    params = params.set(QUERY_PARAM.DATE, (date.getDate()) +
      '-' + (date.getMonth() + 1) +
      '-' + date.getFullYear());
    return this.httpService.getWithParam(BASE_CLASSSTATUS + '/' + this.globalService.currentChild.currentClassId, params);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getClassStatusInMonth(new Date()).pipe(
      map((data) => {
        this.alertService.closeAlert();
        this.classStatus = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/teacher']);
        return of(err);
      })
    );
  }
}
