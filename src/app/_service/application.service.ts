import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs/index';
import {HttpService} from './http.service';
import {AlertService} from './alert.service';
import {BASE_APPLICATION} from '../_constant/path.constant';
import {catchError, map} from 'rxjs/internal/operators';
import {Application} from '../_models/application';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class ApplicationService implements Resolve<any> {
  applications: Application[];
  applicationBehaviorSubject: Subject<string>;

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData(this.globalService.currentChild.currentClassId, this.globalService.currentChild.id);
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router,
              private globalService: GlobalService) {
    this.applicationBehaviorSubject  = new Subject();
  }

  getIndividualApplications(classId: string, childId: string) {
    return this.httpService.get(BASE_APPLICATION + '/' + classId + '/' + childId);
  }

  getData(classId: string, childId: string) {
    this.alertService.alertLoading('Loading...');
    return this.getIndividualApplications(classId, childId).pipe(
      map((data: Application[]) => {
        this.applications = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/home']);
        return of(err);
      })
    );
  }

  createApplication(application: Application) {
    return this.httpService.post(BASE_APPLICATION, application);
  }
}
