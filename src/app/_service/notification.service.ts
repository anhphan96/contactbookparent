import {Injectable} from '@angular/core';
import {Notification} from '../_models/notification';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {HttpService} from './http.service';
import {AlertService} from './alert.service';
import {BASE_NOTIFICATION} from '../_constant/path.constant';
import {catchError, map} from 'rxjs/internal/operators';
import {Observable, of} from 'rxjs/index';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class NotificationService implements Resolve<any> {
  notifications: Notification[];

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private alertService: AlertService,
              private router: Router,
              private globalService: GlobalService) {
  }

  public getNotifications() {
    return this.httpService.get(BASE_NOTIFICATION + '/' + this.globalService.currentChild.currentClassId);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getNotifications().pipe(
      map((data: Notification[]) => {
        this.notifications = data;
        this.alertService.closeAlert();
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/teacher']);
        return of(err);
      })
    );
  }
}
