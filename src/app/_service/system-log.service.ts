import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Observable, of, Subject} from "rxjs/index";
import {SystemLog} from "../_models/system-log";
import {HttpService} from "./http.service";
import {AlertService} from "./alert.service";
import {BASE_BACKEND_URL, BASE_SYSTEMLOG} from "../_constant/path.constant";
import {catchError, map} from "rxjs/internal/operators";
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class SystemLogService implements Resolve<any> {
  systemLogs: SystemLog[];
  systemLogSubject: Subject<SystemLog>;

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.getData();
  }

  constructor(private httpService: HttpService,
              private router: Router,
              private alertService: AlertService,
              private globalService: GlobalService) {
    this.systemLogSubject = new Subject();
  }

  getSystemLogsByChildId() {
    return this.httpService.get(BASE_SYSTEMLOG + '/' + this.globalService.currentChild.id);
  }

  private getData() {
    this.alertService.alertLoading('Loading...');
    return this.getSystemLogsByChildId().pipe(
      map((data: SystemLog[]) => {
        this.alertService.closeAlert();
        this.systemLogs = data;
      }),
      catchError(err => {
        this.alertService.closeAlert();
        this.router.navigate(['/teacher']);
        return of(err);
      })
    );
  }
}
