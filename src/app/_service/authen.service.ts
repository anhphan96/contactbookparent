import {Injectable} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthenService {

  classId: string;

  constructor(private keycloakService: KeycloakService) {
    const role = this.keycloakService.getUserRoles()[0];
    this.classId = role.substr(6, role.length);
  }
}
