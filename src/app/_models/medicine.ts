export class Medicine {
  name: string;

  dosage: string;

  usage: string;
}
