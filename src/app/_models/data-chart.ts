export class DataChart {
  name: string;
  series: Serie[];
}

export class Serie {
  name: string;
  value: number;
}
