import {Photo} from './photo';

export class Album {
  id: string;

  classId: string;

  teacherId: string;

  title: string;

  images: Photo[];

  createdAt: Date;
}
