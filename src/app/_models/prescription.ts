import {Process} from "./process";
import {Medicine} from "./medicine";

export class Prescription {
  id: string;

  childId: string;

  classId: string;

  childName: string;

  pathological: string;

  confirm: boolean;

  fromDay: Date;

  toDay: Date;

  complete: boolean;

  progresses: Process[];

  medicines: Medicine[];

  createdAt: Date;

  updatedAt: Date;
}
