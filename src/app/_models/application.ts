export class Application {
  id: string;

  childId: string;

  classId: string;

  confirm: boolean;

  reason: string;

  fromDate: Date;

  toDate: Date;

  childName: string;

  index: number;
}
