export class Menu {
  morning: string;

  lunch: string;

  afternoon: string;

  title: string;

  date: string;

  weekday: string;
}
