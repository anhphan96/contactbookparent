export class SystemLog {
  createdAt: Date;
  content: string;
  icon: string;
  childId: string;
  seen: boolean;
  dynamicObject: any;
}
