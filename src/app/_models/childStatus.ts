import {Hygience} from "./hygience";

export class ChildStatus {
  id: string;

  childId: string;

  hygience: Hygience;

  childName: string;

  createdAt: Date;

  updatedAt: Date;

  index: number;
}
