export class Children {
  id: string;

  name: string;

  birthDay: Date;

  age: number;

  gender: string;

  parentsId: string;

  classId: string[];

  currentClassId: string;

  createdAt: Date;

  updatedAt: Date;
}
