export class Health {
  id: string;
  childId: string;
  name: string;
  weight: number;
  heigh: number;
  age: number;
  createdAt: Date;
  updatedAt: Date;
  highStandardOfHeight: number;
  highStandardOfWeight: number;
  lowStandardOfHeight: number;
  lowStandardOfWeight: number;
}
