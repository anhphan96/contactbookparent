import {Menu} from './menu';
import {Schedule} from "./schedule";

export class ClassStatus {
  id: string;

  classId: string;

  menu: Menu;

  createdAt: Date;

  schedule: Schedule;
}
