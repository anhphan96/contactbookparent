import {Photo} from "./photo";

export class Notification {
  id: string;

  classId: string;

  teacherId: string;

  teacherName: string;

  image: Photo;

  title: string;

  content: string;

  createdAt: Date;

  updatedAt: Date;
}
