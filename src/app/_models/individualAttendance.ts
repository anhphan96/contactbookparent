export class IndividualAttendance {
  childrentId: string;
  classId: string;
  date: Date;
  attendanceStatus: string;
}
