export class Photo {
  url: string;

  title: string;

  base64: string;
}
