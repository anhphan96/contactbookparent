import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NotificationComponent} from './components/notification/notification.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  DateAdapter, MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import {HeaderComponent} from './components/share/header/header.component';
import {FullCalendarModule} from 'ng-fullcalendar';
import {AttendanceComponent} from './components/attendance/attendance.component';
import {HttpClientModule} from '@angular/common/http';
import {DateFormat} from './dateFormat';
import {AttendanceService} from './_service/attendance.service';
import {AbsentApplicationComponent} from './components/absent-application/absent-application.component';
import {ApplicationService} from './_service/application.service';
import {CreateApplicationDialogComponent} from './components/absent-application/create-application-dialog/create-application-dialog.component';
import {FormsModule} from '@angular/forms';
import {ChattingComponent} from './components/chatting/chatting.component';
import {TalkJsService} from './_service/talkjs.service';
import {MenusComponent} from './components/menus/menus.component';
import {ClassStatusService} from './_service/class-status.service';
import {AlbumsComponent} from './components/albums/albums.component';
import {PicturesComponent} from './components/albums/pictures/pictures.component';
import {ListComponent} from './components/albums/list/list.component';
import {AlbumsService} from './_service/albums.service';
import {HygieneComponent} from './components/hygiene/hygiene.component';
import {HygieneService} from "./_service/hygiene.service";
import {PrescriptionComponent} from './components/prescription/prescription.component';
import {PrescriptionService} from "./_service/prescription.service";
import {CreatPresComponent} from './components/prescription/creat-pres/creat-pres.component';
import {ProgressComponent} from './components/prescription/progress/progress.component';
import {ToastrModule} from 'ngx-toastr';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import {HealthComponent} from './components/health/health.component';
import {HealthService} from './_service/health.service';
import {ChartComponent} from './components/health/chart/chart.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {EditRecordDialogComponent} from './components/health/edit-record-dialog/edit-record-dialog.component';
import {ActivitiesComponent} from './components/activities/activities.component';
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";
import {initializer} from './utils/app-init';
import {NotificationService} from './_service/notification.service';
import {LoginfoComponent} from './components/loginfo/loginfo.component';
import {IgxAvatarModule, IgxFilterModule, IgxIconModule, IgxInputGroupModule, IgxListModule} from "igniteui-angular";
import {SystemLogService} from "./_service/system-log.service";
import {NotiQuantityService} from "./_service/noti-quantity.service";
import {AuthGuardService} from "./_guard/auth-guard.service";
import {ErrorpageComponent} from './components/errorpage/errorpage.component';
import {ChoosechildrenComponent} from './components/choosechildren/choosechildren.component';
import {SwitchchildrenComponent} from './components/share/header/switchchildren/switchchildren.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'error', component: ErrorpageComponent},
  {path: 'choose', component: ChoosechildrenComponent},
  {
    path: '', component: ChattingComponent, canActivate: [AuthGuardService], resolve: {chat: TalkJsService}, children: [
    {path: 'home', component: HomeComponent, canActivate: [AuthGuardService]},
    {
      path: 'attendance',
      component: AttendanceComponent,
      canActivate: [AuthGuardService],
      resolve: {attendance: AttendanceService}
    },
    {
      path: 'absent-applications', component: AbsentApplicationComponent,
      canActivate: [AuthGuardService], resolve: {application: ApplicationService}
    },
    {path: 'menus', component: MenusComponent, canActivate: [AuthGuardService], resolve: {menu: ClassStatusService}},
    {
      path: 'albums', component: AlbumsComponent, canActivate: [AuthGuardService], children: [
      {path: 'list', component: ListComponent, canActivate: [AuthGuardService]},
      {
        path: 'detail/:albumId',
        component: PicturesComponent,
        canActivate: [AuthGuardService],
        resolve: {albums: AlbumsService}
      }
    ]
    },
    {path: 'hygiene', component: HygieneComponent, canActivate: [AuthGuardService], resolve: {hygiene: HygieneService}},
    {
      path: 'prescription',
      component: PrescriptionComponent,
      canActivate: [AuthGuardService],
      resolve: {pres: PrescriptionService}
    },
    {path: 'health', component: HealthComponent, canActivate: [AuthGuardService], resolve: {health: HealthService}},
    {
      path: 'activities',
      component: ActivitiesComponent,
      canActivate: [AuthGuardService],
      resolve: {activities: ClassStatusService}
    },
    {
      path: 'notification', component: NotificationComponent,
      canActivate: [AuthGuardService], resolve: {notifications: NotificationService}
    },
    {path: 'log', component: LoginfoComponent, canActivate: [AuthGuardService], resolve: {logs: SystemLogService}}
  ]
  },
  {path: '**', redirectTo: '/home', pathMatch: 'full'}
];

const material_module = [
  BrowserAnimationsModule,
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatStepperModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
];

@NgModule({
  declarations: [
    AppComponent,
    NotificationComponent,
    HomeComponent,
    HeaderComponent,
    AttendanceComponent,
    AbsentApplicationComponent,
    CreateApplicationDialogComponent,
    ChattingComponent,
    MenusComponent,
    AlbumsComponent,
    PicturesComponent,
    ListComponent,
    HygieneComponent,
    PrescriptionComponent,
    CreatPresComponent,
    ProgressComponent,
    HealthComponent,
    ChartComponent,
    EditRecordDialogComponent,
    ActivitiesComponent,
    LoginfoComponent,
    ErrorpageComponent,
    ChoosechildrenComponent,
    SwitchchildrenComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    material_module,
    FullCalendarModule,
    HttpClientModule,
    FormsModule,
    RoundProgressModule,
    ToastrModule.forRoot(),
    NgxChartsModule,
    IgxAvatarModule,
    IgxFilterModule,
    IgxIconModule,
    IgxListModule,
    IgxInputGroupModule,
    KeycloakAngularModule
  ],
  providers: [{provide: DateAdapter, useClass: DateFormat},
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [CreateApplicationDialogComponent,
    CreatPresComponent,
    ProgressComponent,
    EditRecordDialogComponent,
    SwitchchildrenComponent]
})
export class AppModule {
  constructor(private dateAdapter: DateAdapter<MatDatepickerModule>) {
    dateAdapter.setLocale('en-in'); // DD/MM/YYYY
  }
}
