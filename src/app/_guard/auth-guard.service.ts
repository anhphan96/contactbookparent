import {Injectable} from '@angular/core';
import {KeycloakAuthGuard, KeycloakService} from 'keycloak-angular';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {GlobalService} from '../_service/global.service';
import {Children} from '../_models/children';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService extends KeycloakAuthGuard {

  constructor(protected router: Router, protected keycloakAngular: KeycloakService,
              private globalService: GlobalService) {
    super(router, keycloakAngular);
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (!this.authenticated) {
        this.keycloakAngular.login();
        return;
      }

      if (isNullOrUndefined(this.globalService.currentChild)) {
        this.globalService.getChildrenInfoByUsername().subscribe((data: Children[]) => {
          if (data.length > 0) {
            if (data.length > 1) {
              this.globalService.listChildren = data;
              this.router.navigate(['choose']);
              return resolve(false);
            }
            this.globalService.currentChild = data[0];
            return resolve(true);
          }
          this.router.navigate(['error']);
          return resolve(false);
        }, error => {
          this.router.navigate(['error']);
          return resolve(false);
        });
      } else {
        return resolve(true);
      }

    });
  }
}
