import {Component, OnInit} from '@angular/core';
import {PrescriptionService} from '../../_service/prescription.service';
import {Prescription} from '../../_models/prescription';
import {MatDialog} from '@angular/material';
import {CreatPresComponent} from './creat-pres/creat-pres.component';
import {isNullOrUndefined} from 'util';
import {ProgressComponent} from './progress/progress.component';
import * as _ from 'lodash';
import {ToastrService} from 'ngx-toastr';
import {NotiQuantityService} from "../../_service/noti-quantity.service";

@Component({
  selector: 'app-prescription',
  templateUrl: './prescription.component.html',
  styleUrls: ['./prescription.component.css']
})
export class PrescriptionComponent implements OnInit {
  prescriptions: Prescription[];

  constructor(private prescriptionService: PrescriptionService,
              public dialog: MatDialog,
              private toast: ToastrService,
              private notiQuantityService: NotiQuantityService) {
  }

  ngOnInit() {
    this.prescriptions = this.prescriptionService.prescriptions;
    this.notiQuantityService.notiQuantityForPres.next(this.prescriptions.filter(p => !p.complete).length);
    this.prescriptionService.prescriptionBehaviorSubject.subscribe((data: Prescription) => {
      if (!isNullOrUndefined(data)) {
        const updatePrescription = this.prescriptions.filter(a => a.id === data.id);
        if (!isNullOrUndefined(updatePrescription) && updatePrescription.length > 0) {
          const index = this.prescriptions.indexOf(updatePrescription[0]);
          if (index > -1) {
            this.prescriptions[index] = data;
          }
        }
        this.notiQuantityService.notiQuantityForPres.next(this.prescriptions.filter(p => !p.complete).length);
      }
    });
    window.scroll(0, 0);
  }

  public getWeekdays(date: string) {
    const dateInfo = date.split('-');
    return (new Date(dateInfo[0] + '-' + dateInfo[1] + '-' + dateInfo[2]) + '').substr(0, 3);
  }

  openDialog() {
    const dialogRef = this.dialog.open(CreatPresComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe((data: Prescription) => {
      if (!isNullOrUndefined(data)) {
        this.prescriptions.unshift(data);
        this.toast.success('Your prescription is sent', 'Successfully!');
      }
    });
  }

  openProgress(id: String) {
    const presOut = _.cloneDeep(this.prescriptions.filter(p => p.id === id)[0]);
    this.dialog.open(ProgressComponent, {
      width: '400px',
      data: presOut
    });
  }
}
