import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialogRef, MatTableDataSource} from '@angular/material';
import {AlertService} from '../../../_service/alert.service';
import {Medicine} from '../../../_models/medicine';
import {Prescription} from '../../../_models/prescription';
import {Process} from '../../../_models/process';
import {PrescriptionService} from '../../../_service/prescription.service';
import {isNullOrUndefined} from 'util';
import {GlobalService} from "../../../_service/global.service";

@Component({
  selector: 'app-creat-pres',
  templateUrl: './creat-pres.component.html',
  styleUrls: ['./creat-pres.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CreatPresComponent implements OnInit {
  displayedColumns: string[] = ['No', 'name', 'dosage', 'usage', 'del'];
  dataSource: MatTableDataSource<Medicine>;
  newPres: Prescription;
  newMedicine: Medicine;
  minDate: Date;

  constructor(public dialogRef: MatDialogRef<CreatPresComponent>,
              private alertService: AlertService,
              private prescriptionService: PrescriptionService,
              private globalService: GlobalService) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource([]);
    this.newPres = new Prescription();
    this.newPres.medicines = [];
    this.newPres.progresses = [];
    this.newPres.childId = this.globalService.currentChild.id;
    this.newMedicine = new Medicine();
    this.minDate = new Date();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSave() {
    this.prescriptionService.createPrescription(this.newPres).subscribe((data: Prescription) => {
      this.dialogRef.close(data);
    }, error => {
      this.alertService.alertError('Can not create Prescription, please try again!');
    });
  }

  setFromDate(from) {
    const dateInfo = from.toString().split('/');
    let calDate;
    calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
    this.newPres.fromDay = calDate;
  }

  setToDate(to) {
    const dateInfo = to.toString().split('/');
    let calDate;
    calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0]);
    this.newPres.toDay = calDate;
  }

  addMedicine() {
    if (isNullOrUndefined(this.newMedicine.name)) {
      this.alertService.alertError('Please inform name of medicine first');
      return;
    }
    const medicine = new Medicine();
    medicine.name = this.newMedicine.name;
    medicine.usage = this.newMedicine.usage;
    medicine.dosage = this.newMedicine.dosage;
    this.newPres.medicines.push(medicine);
    this.dataSource = new MatTableDataSource(this.newPres.medicines);
  }

  delMedicines(i: number) {
    this.newPres.medicines.splice(i, 1);
    this.dataSource = new MatTableDataSource(this.newPres.medicines);
  }
}
