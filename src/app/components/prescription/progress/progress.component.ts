import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Medicine} from '../../../_models/medicine';
import {Prescription} from '../../../_models/prescription';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  displayedColumns: string[] = ['No', 'name', 'dosage', 'usage'];
  dataSource: MatTableDataSource<Medicine>;
  prescription: Prescription;
  current: number;
  max: number;

  constructor(public dialogRef: MatDialogRef<ProgressComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Prescription) {
  }

  ngOnInit() {
    this.prescription = this.data;
    this.dataSource = new MatTableDataSource(this.prescription.medicines);
    this.onReloadProcess();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onReloadProcess() {
    this.current = this.prescription.progresses.filter(p => p.confirm === true).length;
    this.max = this.prescription.progresses.length;
  }

}
