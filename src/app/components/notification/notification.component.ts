import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../../_service/notification.service';
import {Notification} from "../../_models/notification";
import {PHOTOS_URL} from "../../_constant/path.constant";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  notifications: Notification[];
  photosUrl: string;
  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.photosUrl = PHOTOS_URL;
    this.notifications = this.notificationService.notifications;
  }

}
