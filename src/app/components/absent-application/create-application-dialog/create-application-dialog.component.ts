import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {AlertService} from '../../../_service/alert.service';
import {isNullOrUndefined} from 'util';
import {Application} from '../../../_models/application';
import {GlobalService} from "../../../_service/global.service";

@Component({
  selector: 'app-create-application-dialog',
  templateUrl: './create-application-dialog.component.html',
  styleUrls: ['./create-application-dialog.component.css']
})
export class CreateApplicationDialogComponent implements OnInit {
  defaultDate: Date;
  minDate: Date;
  reason: '';
  from: Date;
  to: Date;
  constructor(public dialogRef: MatDialogRef<CreateApplicationDialogComponent>,
              private alertService: AlertService,
              private globalService: GlobalService) {
  }

  ngOnInit() {
    this.defaultDate = new Date();
    this.minDate = new Date;
    this.from = this.defaultDate;
    this.to = this.defaultDate;
  }

  onCancel() {
    this.dialogRef.close();
  }

  onCreate(from: Date, to: Date) {
    if (isNullOrUndefined(this.reason)) {
      this.alertService.alertError('Enter the reason first!');
      return;
    }
    const application = new Application();
    const fromDateInfo = from.toString().split('/');
    application.fromDate = new Date(fromDateInfo[2] + '-' + fromDateInfo[1] + '-' + fromDateInfo[0]);
    application.fromDate.setDate(application.fromDate.getDate() + 1);

    const toDateInfo = to.toString().split('/');
    application.toDate = new Date(toDateInfo[2] + '-' + toDateInfo[1] + '-' + toDateInfo[0]);
    application.toDate.setDate(application.toDate.getDate() + 1);

    application.reason = this.reason;
    application.confirm = false;
    application.classId = this.globalService.currentChild.currentClassId;
    application.childId = this.globalService.currentChild.id;
    this.dialogRef.close(application);
  }
}
