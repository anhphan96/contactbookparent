import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AlertService} from '../../_service/alert.service';
import {ApplicationService} from '../../_service/application.service';
import {Application} from '../../_models/application';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ViewChild} from '@angular/core';
import {CreateApplicationDialogComponent} from "./create-application-dialog/create-application-dialog.component";
import {isNullOrUndefined} from "util";
import {NotiQuantityService} from "../../_service/noti-quantity.service";

@Component({
  selector: 'app-absent-application',
  templateUrl: './absent-application.component.html',
  styleUrls: ['./absent-application.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AbsentApplicationComponent implements OnInit {
  defaultDate: Date;
  applications: Application[];
  dataSource: MatTableDataSource<Application>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['fromDate', 'toDate', 'reason'];

  constructor(private alertService: AlertService,
              private applicationService: ApplicationService,
              public dialog: MatDialog,
              private notiQuantityService: NotiQuantityService) {
  }

  ngOnInit() {
    this.alertService.closeAlert();
    this.applications = this.applicationService.applications;
    this.defaultDate = new Date();
    this.dataSource = new MatTableDataSource(this.applications);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.notiQuantityService.notiQuantityForApplication.next(this.applications.filter(a => !a.confirm).length);
    this.applicationService.applicationBehaviorSubject.subscribe((data: string) => {
      if (!isNullOrUndefined(data)) {
        const updateApplication = this.applications.filter(a => a.id === data);
        if (!isNullOrUndefined(updateApplication) && updateApplication.length > 0) {
          updateApplication[0].confirm = true;
          this.dataSource = new MatTableDataSource(this.applications);
          this.dataSource.paginator = this.paginator;
          this.notiQuantityService.notiQuantityForApplication.next(this.applications.filter(a => !a.confirm).length);
        }
      }
    });
  }

  onCreateApplication() {
    this.dialog.open(CreateApplicationDialogComponent, {
      width: '450px'
    }).afterClosed().subscribe((data: Application) => {
      if (!isNullOrUndefined(data)) {
        this.applicationService.createApplication(data).subscribe((returnApp: Application) => {
            this.applicationService.applications.unshift(returnApp);
            this.alertService.alertSuccess('Create application success');
            this.dataSource = new MatTableDataSource(this.applications);
            this.dataSource.paginator = this.paginator;
          },
          error => {
            this.alertService.alertError('Can not create application!');
          });
      }
    });
  }
}
