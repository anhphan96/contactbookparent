import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Options} from 'fullcalendar';
import {AlertService} from '../../_service/alert.service';
import {IndividualAttendance} from '../../_models/individualAttendance';
import {AttendanceService} from '../../_service/attendance.service';
import {ViewChild} from '@angular/core';
import {CalendarComponent} from 'ng-fullcalendar';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AttendanceComponent implements OnInit {
  events = [];
  calendarOptions: Options;
  defaultDate;
  individualAttendances: IndividualAttendance[];
  @ViewChild('ucCalendar') ucCalendar: CalendarComponent;

  constructor(private alertService: AlertService,
              private attendanceService: AttendanceService) {
  }

  ngOnInit() {
    this.alertService.closeAlert();
    this.individualAttendances = this.attendanceService.individualAttendances;
    this.defaultDate = new Date();
    this.getEventsFromIndividualAttendances();
    this.initCalendar();
  }

  private initCalendar() {
    this.calendarOptions = {
      editable: false,
      eventLimit: false,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: ''
      },
      events: this.events,
      displayEventTime: false
    };
  }

  goToDate(date) {
    const dateInfo = date.toString().split('/');
    let calDate;
    if (+dateInfo[0] === 1) {
      calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + 1);
    } else {
      calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + (+dateInfo[0] - 1));
    }
    this.ucCalendar.fullCalendar('gotoDate', calDate);
    this.attendanceService.getIndividualAttendances(calDate).subscribe((data: IndividualAttendance[]) => {
      this.attendanceService.individualAttendances = data;
      this.individualAttendances = data;
      this.getEventsFromIndividualAttendances();
      this.refreshCalendar();
    }, error => {
      this.alertService.alertError('Something went wrong!');
    });
  }

  private getEventsFromIndividualAttendances() {
    this.events = [];
    this.individualAttendances.forEach(i => {
      let color: string;
      if (i.attendanceStatus === 'APPEAR') {
        color = 'green';
      } else if (i.attendanceStatus === 'ALLOWED') {
        color = 'purple';
      } else {
        color = 'red';
      }
      const event = {title: '', start: i.date, end: i.date, color: color};
      this.events.push(event);
    });
  }

  private refreshCalendar() {
    this.ucCalendar.fullCalendar('removeEvents');
    this.ucCalendar.fullCalendar('renderEvents', this.events);
  }

  updateCalendarWhenClickButton(event: any) {
    const dateInfo = event.detail.data._i;
    this.defaultDate = new Date(dateInfo[0] + '-' + (dateInfo[1] + 1) + '-' + dateInfo[2]);
    this.attendanceService.getIndividualAttendances(this.defaultDate).subscribe((data: IndividualAttendance[]) => {
        this.attendanceService.individualAttendances = data;
        this.individualAttendances = data;
        this.getEventsFromIndividualAttendances();
        this.refreshCalendar();
      },
      error => {
        this.alertService.alertError('Can not get Menu!');
      });
  }
}
