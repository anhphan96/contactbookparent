import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ClassStatus} from '../../_models/classStatus';
import {ClassStatusService} from '../../_service/class-status.service';
import {Menu} from '../../_models/menu';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MenusComponent implements OnInit {
  classStatus: ClassStatus[];
  weekdays: string[];
  menu: Menu[];

  constructor(private classStatusService: ClassStatusService) {
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.classStatus = this.classStatusService.classStatus;
    this.weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    this.initMenu();
    this.getMenuFromClassStatus();
  }

  private getMenuFromClassStatus() {
    let i = 0;
    this.classStatus.forEach(c => {
      if (!isNullOrUndefined(c.menu)) {
        let mn = new Menu();
        mn = c.menu;
        const dateInfo = (c.createdAt + '').substring(0, 10).split('-');
        const existMenu = this.menu.filter(m => m.weekday ===
          this.getWeekdays(new Date(dateInfo[0] + '-' + dateInfo[1] + '-' + dateInfo[2])))[0];
        if (!isNullOrUndefined(existMenu)) {
          existMenu.morning = mn.morning;
          existMenu.afternoon = mn.afternoon;
          existMenu.lunch = mn.lunch;
          existMenu.date = this.getDateString(c.createdAt);
        }
      }
    });
  }

  private getDateString(date: Date) {
    const dateInfo = (date + '').substring(0, 10).split('-');
    return dateInfo[2] + '-' + dateInfo[1] + '-' + dateInfo[0];
  }

  private getWeekdays(date: Date) {
    return (date + '').substr(0, 3);
  }

  private initMenu() {
    this.menu = [];
    this.weekdays.forEach(w => {
      const mn = new Menu();
      mn.weekday = w;
      this.menu.push(mn);
    });
  }
}
