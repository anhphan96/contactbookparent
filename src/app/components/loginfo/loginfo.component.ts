import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {SystemLog} from '../../_models/system-log';
import {SystemLogService} from '../../_service/system-log.service';
import {isNullOrUndefined} from 'util';
import {NotiQuantityService} from '../../_service/noti-quantity.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-loginfo',
  templateUrl: './loginfo.component.html',
  styleUrls: ['./loginfo.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginfoComponent implements OnInit {
  systemLogs: SystemLog[];

  constructor(private systemLogService: SystemLogService,
              private notiquantityService: NotiQuantityService,
              private router: Router) {
  }

  ngOnInit() {
    // this.notiquantityService.notiQuanityObject.systemlog = 0;
    // this.notiquantityService.notiQuantity.next(this.notiquantityService.notiQuanityObject);
    this.systemLogs = this.systemLogService.systemLogs;
    console.log(this.systemLogs);
    this.systemLogService.systemLogSubject.subscribe((data: SystemLog) => {
      if (!isNullOrUndefined(data)) {
        this.systemLogs.unshift(data);
      }
    });
  }

  public navigate(icon: string) {
    switch (icon) {
      case 'photo_album':
        this.router.navigate(['albums', 'list']);
        break;
      case 'sentiment_very_dissatisfied':
        this.router.navigate(['absent-applications']);
        break;
      case 'add_box':
        this.router.navigate(['health']);
        break;
      case 'assignment':
        this.router.navigate(['prescription']);
        break;
      case 'notification_important':
        this.router.navigate(['notification']);
        break;
      default:
        return;
    }
  }
}
