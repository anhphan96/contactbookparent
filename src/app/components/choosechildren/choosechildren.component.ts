import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../_service/global.service";
import {Router} from "@angular/router";
import {isNullOrUndefined} from "util";
import {Children} from "../../_models/children";

@Component({
  selector: 'app-choosechildren',
  templateUrl: './choosechildren.component.html',
  styleUrls: ['./choosechildren.component.css']
})
export class ChoosechildrenComponent implements OnInit {
  children: Children[];
  constructor(private globalService: GlobalService,
              private router: Router) { }

  ngOnInit() {
    if (!isNullOrUndefined(this.globalService.currentChild) || isNullOrUndefined(this.globalService.listChildren)) {
      console.log('home');
      this.router.navigate(['home']);
    }
    this.children = this.globalService.listChildren;
  }

  public choose(childId: string) {
    this.globalService.currentChild = this.children.filter(c => c.id === childId)[0];
    this.router.navigate(['home']);
  }
}
