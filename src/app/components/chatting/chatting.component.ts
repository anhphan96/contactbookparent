import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as Talk from 'talkjs';
import {TalkJsService} from '../../_service/talkjs.service';
import {isNullOrUndefined} from 'util';
import {NotiQuantityService} from '../../_service/noti-quantity.service';
import {NotiQuantity} from '../../_models/noti-quantity';
import {ApplicationService} from '../../_service/application.service';
import {SystemLogService} from '../../_service/system-log.service';
import {PrescriptionService} from '../../_service/prescription.service';
import {KeycloakService} from 'keycloak-angular';
import {GlobalService} from '../../_service/global.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {SystemLog} from '../../_models/system-log';
import {Prescription} from "../../_models/prescription";

@Component({
  selector: 'app-chatting',
  templateUrl: './chatting.component.html',
  styleUrls: ['./chatting.component.css']
})
export class ChattingComponent implements OnInit {
  private session: Talk.Session;
  private currentConversation: Talk.ConversationBuilder;
  private popup: Talk.Popup;
  public numberOfNoti: number;
  private stompClient;
  constructor(private talkJs: TalkJsService,
              private applicaionService: ApplicationService,
              private systemLogService: SystemLogService,
              private prescriptionService: PrescriptionService,
              private notiquantityService: NotiQuantityService,
              private keyCloakService: KeycloakService,
              private globalService: GlobalService,
              private toast: ToastrService,
              private route: Router) {
  }

  ngOnInit() {
    this.toast.toastrConfig.timeOut = 10000;
    this.keyCloakService.getToken().then((data) => this.initializeAbsentApplicationConnection(data));
    this.notiquantityService.getNotiQuantity()
      .subscribe((data: NotiQuantity) => {
        this.numberOfNoti = data.systemlog;
        this.notiquantityService.notiQuantity.next(data);
        this.notiquantityService.notiQuanityObject = data;
      });
    this.notiquantityService.notiQuantity
      .subscribe((data: NotiQuantity) => {
        this.numberOfNoti = data.systemlog;
      });
    if (this.talkJs.getCurrentUsername()) {
      this.talkJs.getSession().then(session => {
        this.session = session;
      });
    }
  }

  startConversation() {
    if (isNullOrUndefined(this.currentConversation)) {
      const me = this.talkJs.getCurrentUser();
      const other = new Talk.User({id: 'Anh', name: 'Anh'});
      this.currentConversation = this.session.getOrCreateConversation(Talk.oneOnOneId(me, other));
      this.currentConversation.setParticipant(me);
      this.currentConversation.setParticipant(other);
      this.popup = this.session.createPopup(this.currentConversation);
      this.popup.mount({show: true});
      return;
    }
    console.log('just show');
    this.popup = this.session.createPopup(this.currentConversation);
    this.popup.mount({show: true});
  }

  navigateToLog() {
    this.notiquantityService.notiQuanityObject.systemlog = 0;
    this.numberOfNoti = 0;
  }

  initializeAbsentApplicationConnection(token: string) {
    const ws = new WebSocket('ws://localhost:7070/greet?access_token=bearer%20' + token);
    this.stompClient = Stomp.over(ws);
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/application/' + that.globalService.currentChild.id, (message) => {
        if (message.body) {
          const systemLog = JSON.parse(message.body) as SystemLog;

          const applicationId = systemLog.dynamicObject as string;
          that.applicaionService.applicationBehaviorSubject.next(applicationId);
          that.systemLogService.systemLogSubject.next(systemLog);
          that.toast.info(systemLog.content, '').onTap.subscribe(() => {
            that.navigate('absent-applications');
          });
          if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
            that.notiquantityService.notiQuanityObject.systemlog++;
            that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
          }
        }
      });

      that.stompClient.subscribe('/album/' + that.globalService.currentChild.currentClassId, (message) => {
        if (message.body) {
          const systemLog = JSON.parse(message.body) as SystemLog;
          that.systemLogService.systemLogSubject.next(systemLog);
          that.toast.info(systemLog.content, '').onTap.subscribe(() => {
            that.navigate('albums/list');
          });
          if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
            that.notiquantityService.notiQuanityObject.systemlog++;
            that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
          }
        }
      });

      that.stompClient.subscribe('/health/' + that.globalService.currentChild.id, (message) => {
        if (message.body) {
          const systemLog = JSON.parse(message.body) as SystemLog;
          that.systemLogService.systemLogSubject.next(systemLog);
          that.toast.info(systemLog.content, '').onTap.subscribe(() => {
            that.navigate('health');
          });
          if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
            that.notiquantityService.notiQuanityObject.systemlog++;
            that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
          }
        }
      });

      that.stompClient.subscribe('/prescription/' + that.globalService.currentChild.id, (message) => {
        if (message.body) {
          const systemLog = JSON.parse(message.body) as SystemLog;
          const prescriptionId = systemLog.dynamicObject as Prescription;
          that.systemLogService.systemLogSubject.next(systemLog);
          that.prescriptionService.prescriptionBehaviorSubject.next(prescriptionId);
          that.toast.info(systemLog.content, '').onTap.subscribe(() => {
            that.navigate('prescription');
          });
          if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
            that.notiquantityService.notiQuanityObject.systemlog++;
            that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
          }
        }
      });

      that.stompClient.subscribe('/noti/' + that.globalService.currentChild.id, (message) => {
        if (message.body) {
          const systemLog = JSON.parse(message.body) as SystemLog;
          that.systemLogService.systemLogSubject.next(systemLog);
          that.toast.info(systemLog.content, '').onTap.subscribe(() => {
            that.navigate('notification');
          });
          if (!isNullOrUndefined(that.notiquantityService.notiQuanityObject)) {
            that.notiquantityService.notiQuanityObject.systemlog++;
            that.notiquantityService.notiQuantity.next(that.notiquantityService.notiQuanityObject);
          }
        }
      });
    });
  }

  navigate(url: string) {
    this.route.navigate([url]);
  }
}
