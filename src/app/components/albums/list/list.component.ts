import {
  AfterViewInit, Component, OnInit,
  ViewEncapsulation
} from '@angular/core';
import {Router} from '@angular/router';
import {AlbumsService} from '../../../_service/albums.service';
import {Album} from '../../../_models/album';
import {PHOTOS_URL} from '../../../_constant/path.constant';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit, AfterViewInit {
  tabActive = 0;
  albums: Album[];
  imageUrl = PHOTOS_URL;
  constructor(private router: Router,
              private albumService: AlbumsService) {
  }

  ngOnInit() {
    this.tabActive = this.albumService.selectedIndex;
    this.albumService.albums.subscribe((alb: Album[]) => {
      this.albums = alb;
    });
  }

  ngAfterViewInit(): void {
    this.loadThrParty();
  }

  loadThrParty(): void {
    document.getElementById('light').remove();
    document.getElementById('light1').remove();
    const testScript = document.createElement('script');
    testScript.setAttribute('id', 'light');
    testScript.setAttribute('src', './assets/gallery/lightgallery-all.js');
    document.body.appendChild(testScript);

    const testScript1 = document.createElement('script');
    testScript1.setAttribute('id', 'light1');
    testScript1.setAttribute('src', './assets/gallery/image-gallery.js');
    document.body.appendChild(testScript1);
  }

  navigateToListPhotos(albumId: string) {
    this.router.navigate(['albums', 'detail', albumId]);
  }

  handleFragment(event: any) {
    if (event.index === 0) {
      this.loadThrParty();
      this.router.navigate(['albums', 'list']);
    } else {
      this.router.navigate(['albums', 'list'], {fragment: 'list'});
    }
  }
}
