import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AlbumsService} from '../../../_service/albums.service';
import {Album} from '../../../_models/album';
import {PHOTOS_URL} from '../../../_constant/path.constant';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit, AfterViewInit {
  a = 'https://www.gettyimages.com/gi-resources/images/Embed/new/embed2.jpg';
  @ViewChild('sidenav') sidenav: any;
  albums: Album[];
  currentAlbum: Album;
  imageUrl = PHOTOS_URL;
  albumId: string;

  constructor(private albumService: AlbumsService,
              private router: ActivatedRoute,
              private route: Router) {
  }

  ngOnInit() {
    this.albums = this.albumService.listAlbums;

    this.albumId = this.router.snapshot.params.sub;
    this.router.params.subscribe(param => {
      this.albumId = param['albumId'];
      this.currentAlbum = this.albums.filter(a => a.id === this.albumId)[0];
    });
    this.currentAlbum = this.albums.filter(a => a.id === this.albumId)[0];
  }

  ngAfterViewInit(): void {
    this.loadThrParty();
    const currentAlb = document.getElementsByClassName(this.currentAlbum.id)[0];
    const offsetTop = (<HTMLElement> currentAlb).offsetTop;
    if (offsetTop > 400) {
      currentAlb.scrollIntoView();
    }
    window.scroll(0, 0);
  }

  loadThrParty(): void {
    document.getElementById('light').remove();
    document.getElementById('light1').remove();
    const testScript = document.createElement('script');
    testScript.setAttribute('id', 'light');
    testScript.setAttribute('src', './assets/gallery/lightgallery-all.js');
    document.body.appendChild(testScript);

    const testScript1 = document.createElement('script');
    testScript1.setAttribute('id', 'light1');
    testScript1.setAttribute('src', './assets/gallery/image-gallery.js');
    document.body.appendChild(testScript1);
  }

  onSwipeRight() {
    this.sidenav.open();
  }

  onSwipeLeft() {
    this.sidenav.close();
  }

  moveToAlbum(albumId: String) {
    this.route.navigateByUrl('/SampleComponent', {skipLocationChange: true}).then(
      () => {
        this.route.navigate(['albums', 'detail', albumId]);
      }
    );
  }
}
