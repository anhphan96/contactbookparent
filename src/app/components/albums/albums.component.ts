import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AlbumsService} from '../../_service/albums.service';
import {Album} from '../../_models/album';
import {AlertService} from '../../_service/alert.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AlbumsComponent implements OnInit {
  constructor(private router: ActivatedRoute,
              private albumService: AlbumsService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.albumService.getAlbums().subscribe((albums: Album[]) => {
        this.albumService.albums.next(albums);
      },
      error => {
        this.alertService.alertError('Something went wrong!');
      });
    window.scroll(0, 0);
    this.router.fragment.subscribe(f => {
      if (f === 'list') {
        this.albumService.selectedIndex = 1;
      } else {
        this.albumService.selectedIndex = 0;
      }
    });
  }
}
