import {Component, OnInit} from '@angular/core';
import {ChildStatus} from '../../_models/childStatus';
import {HygieneService} from '../../_service/hygiene.service';
import {AlertService} from '../../_service/alert.service';

@Component({
  selector: 'app-hygiene',
  templateUrl: './hygiene.component.html',
  styleUrls: ['./hygiene.component.css']
})
export class HygieneComponent implements OnInit {
  public hygieneStatus: ChildStatus[];

  constructor(private hygieneService: HygieneService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.hygieneStatus = this.hygieneService.childrenStatus;
    window.scroll(0, 0);
  }

  public getWeekdays(date: string) {
    const dateInfo = date.split('-');
    return (new Date(dateInfo[0] + '-' + dateInfo[1] + '-' + dateInfo[2]) + '').substr(0, 3);
  }

  goToDate(date) {
    const dateInfo = date.toString().split('/');
    let calDate;
    if (+dateInfo[0] === 1) {
      calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + 1);
    } else {
      calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + (+dateInfo[0] - 1));
    }
    this.hygieneService.getIndividualChildStatus(calDate).subscribe((data: ChildStatus[]) => {
      this.hygieneStatus = data;
      console.log(this.hygieneStatus);
    }, error => {
      this.alertService.alertError('Something went wrong!');
    });
  }
}
