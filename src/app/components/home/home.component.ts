import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {NotiQuantityService} from "../../_service/noti-quantity.service";
import {NotiQuantity} from "../../_models/noti-quantity";
import {isNullOrUndefined} from "util";
import {NotificationService} from "../../_service/notification.service";
import {Notification} from "../../_models/notification";
import {GlobalService} from "../../_service/global.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewInit {
  images: any[];
  notiQuantity: NotiQuantity;
  notifications: Notification[];

  ngAfterViewInit(): void {
    document.getElementById('scroll').remove();
    document.getElementById('scroll1').remove();
    const testScript = document.createElement('script');
    testScript.setAttribute('id', 'scroll');
    testScript.setAttribute('src', './assets/carousel/magicscroll.js');
    document.body.appendChild(testScript);

    const testScript1 = document.createElement('script');
    testScript1.setAttribute('id', 'scroll1');
    testScript1.setAttribute('src', './assets/carousel/perttify.min.js');
    document.body.appendChild(testScript1);
  }

  constructor(private route: Router,
              private notiquantityService: NotiQuantityService,
              private notificationService: NotificationService) {
    this.notiQuantity = new NotiQuantity();
  }

  ngOnInit() {
    this.notiQuantity.systemlog = 0;
    this.notiQuantity.application = 0;
    this.notiQuantity.prescription = 0;
    this.notifications = [];
    if (!isNullOrUndefined(this.notiquantityService.notiQuanityObject)) {
      this.notiQuantity = this.notiquantityService.notiQuanityObject;
    } else {
      this.notiquantityService.notiQuantity.subscribe((data: NotiQuantity) => {
        this.notiQuantity = data;
      });
    }
    this.notiquantityService.notiQuantityForApplication.subscribe((data: number) => {
      this.notiQuantity.application = data;
    });
    this.notiquantityService.notiQuantityForPres.subscribe((data: number) => {
      this.notiQuantity.prescription = data;
    });
    this.notificationService.getNotifications().subscribe((data: Notification[]) => {
      this.notifications = data;
    }, error => {
      console.log('error');
    });
    this.images = [
      {url: 'https://anglicantas.org.au/wp-content/uploads/2017/01/childrens-ministry.jpg'},
      {url: 'https://static1.squarespace.com/static/561531b9e4b05090e8ae0cd0/t/5760118be321408871da85e3/1465913759810/'},
      {url: 'https://www.cdc.gov/features/childrens-preparedness-unit/childrens-preparedness-unit_456px.jpg'},
      {url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4QtEYF3phmAeZq_iNcu876eLjns9YSjHoNwmc5yelyfQYRPEI'},
      {url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRa7tq1-PUMjC1dLLIGOpnwXs6KCIYDS46Ayv0Ecgqn3QltOk1'}];
  }

  customExpand(openPanel: boolean) {
    const icon = document.getElementsByClassName('mat-expansion-indicator')[0];
    if (openPanel) {
      icon.classList.remove('text-align-left');
      icon.classList.add('text-align-right');
      return;
    }

    icon.classList.remove('text-align-right');
    icon.classList.add('text-align-left');
  }

  navigate(url: string) {
    this.route.navigate([url]);
  }

  navigateToAlbum() {
    console.log('cl');
    this.route.navigate(['application']);
  }
}
