import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {GlobalService} from "../../../../_service/global.service";
import {Children} from "../../../../_models/children";
import {Router} from "@angular/router";

@Component({
  selector: 'app-switchchildren',
  templateUrl: './switchchildren.component.html',
  styleUrls: ['./switchchildren.component.css']
})
export class SwitchchildrenComponent implements OnInit {
  children: Children[];
  constructor(public dialogRef: MatDialogRef<SwitchchildrenComponent>,
              private globalService: GlobalService,
              private router: Router) { }

  ngOnInit() {
    this.children = this.globalService.listChildren;
  }

  public choose(childId: string) {
    this.globalService.currentChild = this.children.filter(c => c.id === childId)[0];
    this.dialogRef.close(true);
  }
}
