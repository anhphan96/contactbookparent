import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {KeycloakService} from "keycloak-angular";
import {BASE_LANDING_PAGE} from "../../../_constant/path.constant";
import {GlobalService} from "../../../_service/global.service";
import {Children} from "../../../_models/children";
import {MatDialog} from "@angular/material";
import {SwitchchildrenComponent} from "./switchchildren/switchchildren.component";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  child: Children;
  constructor(private route: Router,
              private keyCloakService: KeycloakService,
              private globalService: GlobalService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.child = this.globalService.currentChild;
  }

  home() {
    this.route.navigate(['home']);
  }

  public logout() {
    this.keyCloakService.clearToken();
    this.keyCloakService.logout(BASE_LANDING_PAGE);
  }

  // @HostListener('window:scroll', ['$event'])
  // onWindowScroll() {
  //   if (window.pageYOffset > 60) {
  //     const elementSideBar = document.getElementById('app-header');
  //     elementSideBar.classList.add('app-header-fixed');
  //   } else {
  //     const elementSideBar = document.getElementById('app-header');
  //     elementSideBar.classList.remove('app-header-fixed');
  //   }
  // }
  swithChildren() {
    const dia = this.dialog.open(SwitchchildrenComponent);
    dia.afterClosed().subscribe((result: boolean) => {
      if (result) {
        console.log('sw');
        this.route.navigateByUrl('/SampleComponent', { skipLocationChange: true }).then(
          () => {
            this.route.navigate(['home']);
          }
        );
      }
    });
  }
}
