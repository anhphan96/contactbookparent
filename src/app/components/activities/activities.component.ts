import {Component, OnInit} from '@angular/core';
import {ClassStatus} from "../../_models/classStatus";
import {Schedule} from "../../_models/schedule";
import {ClassStatusService} from "../../_service/class-status.service";
import {isNullOrUndefined} from "util";
import {AlertService} from "../../_service/alert.service";

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {
  classStatus: ClassStatus[];
  schedules: Schedule[];

  constructor(private classStatusService: ClassStatusService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.classStatus = this.classStatusService.classStatus;
    console.log(new Date());
    this.getScheduleFromClassStatus();
  }

  private getScheduleFromClassStatus() {
    this.schedules = [];
    this.classStatus.forEach(c => {
      if (!isNullOrUndefined(c.schedule)) {
        const sche = new Schedule();
        sche.createdAt = c.createdAt;
        sche.morning = c.schedule.morning;
        sche.afternoon = c.schedule.afternoon;
        this.schedules.push(sche);
      }
    });
  }

  goToDate(date) {
    const dateInfo = date.toString().split('/');
    let calDate;
    if (+dateInfo[0] === 1) {
      calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + 1);
    } else {
      calDate = new Date(dateInfo[2] + '-' + dateInfo[1] + '-' + (+dateInfo[0] - 1));
    }
    this.classStatusService.getClassStatusInMonth(calDate).subscribe((data: ClassStatus[]) => {
      this.classStatus = data;
      this.getScheduleFromClassStatus();
    }, error => {
      this.alertService.alertError('Something went wrong!');
    });
  }

  public getWeekdays(date: string) {
    const dateInfo = date.split('-');
    return (new Date(dateInfo[0] + '-' + dateInfo[1] + '-' + dateInfo[2]) + '').substr(0, 3);
  }

  public getMonth(date: string) {
    const dateInfo = date.split('-');
    return (new Date(dateInfo[0] + '-' + dateInfo[1] + '-' + dateInfo[2]) + '').substr(4, 3);
  }
}
