import { Component, OnInit } from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import {BASE_LANDING_PAGE} from "../../_constant/path.constant";

@Component({
  selector: 'app-errorpage',
  templateUrl: './errorpage.component.html',
  styleUrls: ['./errorpage.component.css']
})
export class ErrorpageComponent implements OnInit {

  constructor(private keyCloakService: KeycloakService) { }

  ngOnInit() {
  }

  public logout() {
    this.keyCloakService.clearToken();
    this.keyCloakService.logout(BASE_LANDING_PAGE);
  }
}
