import {Component, Input, OnInit} from '@angular/core';
import {DataChart} from "../../../_models/data-chart";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  view: any[] = [440, 300];
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Age';
  showYAxisLabel = true;
  yAxisLabel = '';
  autoScale = true;
  @Input('data') dataCharts: DataChart[];
  @Input('type') type: string;
  constructor() { }

  ngOnInit() {
    this.yAxisLabel = this.type;
  }

}
