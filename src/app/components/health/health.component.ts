import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {HealthService} from '../../_service/health.service';
import {Health} from '../../_models/health';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {DataChart, Serie} from "../../_models/data-chart";
import {EditRecordDialogComponent} from "./edit-record-dialog/edit-record-dialog.component";
import {isNullOrUndefined} from "util";
import {AlertService} from "../../_service/alert.service";

@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HealthComponent implements OnInit {

  childrenHealthes: Health[];
  dataSource: MatTableDataSource<Health>;
  healthDisplayedColumns: string[] = ['createdAt', 'age', 'weight', 'heigh'];
  @ViewChild('healthPaginator') healthPaginator: MatPaginator;
  newHealth: Health;
  dataWeightCharts: DataChart[];
  dataHeightCharts: DataChart[];
  constructor(private healthService: HealthService,
              public dialog: MatDialog,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.childrenHealthes = this.healthService.childrenHealthes;
    this.dataSource = new MatTableDataSource(this.childrenHealthes);
    this.dataSource.paginator = this.healthPaginator;
    this.newHealth = new Health();
    if (this.childrenHealthes.length > 0) {
      this.initCurrentWeightCharts();
      this.initCurrentHeightCharts();
    }
    window.scroll(0, 0);
  }

  private initCurrentWeightCharts() {
    const currentDataChart = new DataChart();
    currentDataChart.name = this.childrenHealthes[0].name;
    currentDataChart.series = [];
    const healthClone = Object.assign([], this.childrenHealthes);
    healthClone.reverse().forEach(h => {
      const sr = new Serie();
      sr.name = h.age + '';
      sr.value = h.weight;
      currentDataChart.series.push(sr);
    });
    this.dataWeightCharts = [];
    this.dataWeightCharts.push(currentDataChart);
    this.dataWeightCharts.push(this.initHighStandardWeightCharts());
    this.dataWeightCharts.push(this.initLowStandardWeightCharts());
  }

  private initHighStandardWeightCharts() {
    const highDataChart = new DataChart();
    highDataChart.name = 'Fat';
    highDataChart.series = [];
    const healthClone = Object.assign([], this.childrenHealthes);
    healthClone.reverse().forEach(h => {
      const sr = new Serie();
      sr.name = h.age + '';
      sr.value = h.highStandardOfWeight;
      highDataChart.series.push(sr);
    });
    return highDataChart;
  }

  private initLowStandardWeightCharts() {
    const lowDataChart = new DataChart();
    lowDataChart.name = 'Malnutrition';
    lowDataChart.series = [];
    const healthClone = Object.assign([], this.childrenHealthes);
    healthClone.reverse().forEach(h => {
      const sr = new Serie();
      sr.name = h.age + '';
      sr.value = h.lowStandardOfWeight;
      lowDataChart.series.push(sr);
    });
    return lowDataChart;
  }

  private initCurrentHeightCharts() {
    const currentDataChart = new DataChart();
    currentDataChart.name = this.childrenHealthes[0].name;
    currentDataChart.series = [];
    const healthClone = Object.assign([], this.childrenHealthes);
    healthClone.reverse().forEach(h => {
      const sr = new Serie();
      sr.name = h.age + '';
      sr.value = h.heigh;
      currentDataChart.series.push(sr);
    });
    this.dataHeightCharts = [];
    this.dataHeightCharts.push(currentDataChart);
    this.dataHeightCharts.push(this.initHighStandardHeightCharts());
    this.dataHeightCharts.push(this.initLowStandardHeightCharts());
  }

  private initHighStandardHeightCharts() {
    const highDataChart = new DataChart();
    highDataChart.name = 'High';
    highDataChart.series = [];
    const healthClone = Object.assign([], this.childrenHealthes);
    healthClone.reverse().forEach(h => {
      const sr = new Serie();
      sr.name = h.age + '';
      sr.value = h.highStandardOfHeight;
      highDataChart.series.push(sr);
    });
    return highDataChart;
  }

  private initLowStandardHeightCharts() {
    const lowDataChart = new DataChart();
    lowDataChart.name = 'Short';
    lowDataChart.series = [];
    const healthClone = Object.assign([], this.childrenHealthes);
    healthClone.reverse().forEach(h => {
      const sr = new Serie();
      sr.name = h.age + '';
      sr.value = h.lowStandardOfHeight;
      lowDataChart.series.push(sr);
    });
    return lowDataChart;
  }

  onSaveRecord(id: string) {
    const healthOut = new Health();
    Object.assign(healthOut, this.childrenHealthes.filter(h => h.id === id)[0]);

    const dialogRef = this.dialog.open(EditRecordDialogComponent, {
      width: '350px',
      data: healthOut
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (!isNullOrUndefined(result)) {
        if (result === true) {
          this.healthService.deleteChildrenHeal(id).subscribe(() => {
            const index = this.childrenHealthes.indexOf(this.childrenHealthes.filter(h => h.id === id)[0]);
            this.childrenHealthes.splice(index, 1);
            this.dataSource = new MatTableDataSource(this.childrenHealthes);
            this.dataSource.paginator = this.healthPaginator;
            this.initCurrentWeightCharts();
            this.initCurrentHeightCharts();
          }, error => {
            this.alertService.alertError('Something went wrong, please try again!');
          });
        } else {
          this.healthService.saveChildrenHeal(<Health>result).subscribe((data: Health) => {
              const index = this.childrenHealthes.indexOf(this.childrenHealthes.filter(h => h.id === data.id)[0]);
              this.childrenHealthes[index] = data;
              this.dataSource = new MatTableDataSource(this.childrenHealthes);
              this.dataSource.paginator = this.healthPaginator;
              this.initCurrentWeightCharts();
              this.initCurrentHeightCharts();
            },
            error => {
              this.alertService.alertError('Something went wrong, please try again!');
            });
        }
      }
    });
  }

  createRecord() {
    if (isNullOrUndefined(this.newHealth.age) || this.newHealth.age < 0) {
      this.alertService.alertError('please enter the age!');
      return;
    }
    if (isNullOrUndefined(this.newHealth.weight) && isNullOrUndefined(this.newHealth.heigh)) {
      this.alertService.alertError('please enter at least one record!');
      return;
    }
    this.healthService.saveChildrenHeal(this.newHealth).subscribe((data: Health) => {
      this.childrenHealthes.unshift(data);
      this.dataSource = new MatTableDataSource(this.childrenHealthes);
      this.dataSource.paginator = this.healthPaginator;
      this.initCurrentWeightCharts();
      this.initCurrentHeightCharts();
    }, error => {
      this.alertService.alertError('Something went wrong, please try again!');
    });
  }
}
