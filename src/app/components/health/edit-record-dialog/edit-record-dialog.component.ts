import {Component, Inject, OnInit} from '@angular/core';
import {Health} from "../../../_models/health";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {AlertService} from "../../../_service/alert.service";
import {HealthService} from '../../../_service/health.service';
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-edit-record-dialog',
  templateUrl: './edit-record-dialog.component.html',
  styleUrls: ['./edit-record-dialog.component.css']
})
export class EditRecordDialogComponent implements OnInit {

  health: Health;

  constructor(public dialogRef: MatDialogRef<EditRecordDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Health,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.health = this.data;
  }

  onCancel() {
    this.dialogRef.close();
  }

  edit() {
    if (isNullOrUndefined(this.health.age) || this.health.age < 0) {
      this.alertService.alertError('please enter the age!');
      return;
    }
    if (isNullOrUndefined(this.health.weight) && isNullOrUndefined(this.health.heigh)) {
      this.alertService.alertError('please enter at least one record!');
      return;
    }
    this.dialogRef.close(this.health);
  }

  deleteHealth() {
    this.alertService.alertConfirmation('Are you sure to delete this record?').then(result => {
      if (result.value) {
        this.dialogRef.close(true);
      }
    });
  }

}
